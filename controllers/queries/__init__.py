from controllers.queries.company_queries import get_people_who_work_at_same_company
from controllers.queries.people_queries import get_people_of_age, get_count_of_people_same_first_name, \
                                               get_count_of_people_same_last_name, get_count_of_people_same_first_name_last_name
from controllers.queries.queries import executeQueries
