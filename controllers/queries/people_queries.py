def get_people_of_age(people_collection, age=25):
    return list(people_collection.find({'age': age}, {'_id': 0, 'first_name': 1, 'last_name': 1}))


def get_count_of_people_same_first_name(people_collection):
    pipeline = [
        {'$project': {'_id': 0, 'first_name': 1}},
        {
            '$group': {
                '_id': "$first_name",
                'count': {'$sum': 1}
            }
        },
        {
            '$project': {
                '_id': 0, 'first_name': "$_id", 'count': 1
            }
        },
        {'$sort': {'count': -1}}

    ]
    cursor = people_collection.aggregate(pipeline)
    return list(cursor)


def get_count_of_people_same_last_name(people_collection):
    pipeline = [
        {'$project': {'_id': 0, 'last_name': 1}},
        {
            '$group': {
                '_id': "$last_name",
                'count': {'$sum': 1}
            }
        },
        {
            '$project': {
                '_id': 0, 'last_name': "$_id", 'count': 1
            }
        },
        {'$sort': {'count': -1}}
    ]
    cursor = people_collection.aggregate(pipeline)
    return list(cursor)

def get_count_of_people_same_first_name_last_name(people_collection):
    pipeline = [
        {'$project': {'_id': 0, 'first_name': 1, 'last_name': 1}},
        {
            '$group': {
                "_id": {
                    'first_name': "$first_name",
                    'last_name': "$last_name"
                },
                'count': { '$sum': 1 }
            }
        },
        { "$sort": { 'count': -1 } },
        { "$match": { "count": { "$gte": 5 } } }
    ]

    cursor = people_collection.aggregate(pipeline)
    return list(cursor)