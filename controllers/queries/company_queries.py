from pprint import pprint

def get_people_who_work_at_same_company(people_collection):
    pipeline = [
        {
            '$group': {
                '_id': "$occupation.company_name",
                'people': {'$sum': 1}
             }
        },
        {
            '$match': { 'people': {'$gt': 10 } }
        }
    ]
    people_same_company = list(people_collection.aggregate(pipeline))
    return people_same_company
