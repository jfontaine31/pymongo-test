from pprint import pprint
from controllers.queries import get_people_who_work_at_same_company, get_people_of_age, \
                                get_count_of_people_same_first_name, get_count_of_people_same_last_name, \
                                get_count_of_people_same_first_name_last_name

def executeQueries(people_collection):
    '''
        * How many people work at the same company
        * list of people of said age
        * people with the same first name
        * people with the same last name
        * people with the same first name and last name
        * people with income less than
        * people with income greater than
        * population in the same state
        * mean income of population in the same state
        * people with the same birthday
        * counts of the same
        * What is the Distinct list of the different companies
        * What is the Distinct list of occupations
        * Average salary for people in the same state
        * Mean salary for people in the same state
        * Average salary at said company
        * Mean salary at said company
        * Average salary for occupation
        * Mean salary for occupation
    '''
    pprint(('how many people work at the same company', get_people_who_work_at_same_company(people_collection)))
    pprint(('list of people at said age', get_people_of_age(people_collection, age=120)))
    pprint(('how many people with the same first name', get_count_of_people_same_first_name(people_collection)))
    pprint(('people with the same last name', get_count_of_people_same_last_name(people_collection)))
    pprint(('people with the same first name and last name', get_count_of_people_same_first_name_last_name(people_collection)))
    pprint(('people with income less than number'))
    pprint(('people with income greater than'))
    pprint(('population in the same state'))
    pprint(('population of with income'))
    pprint(('people with the same birthday'))
    pprint(('counts of the same'))
    pprint(('What is the Distinct list of the different companies'))
    pprint(('What is the Distinct list of occupations'))
    pprint(('Average salary for people in the same state'))
    pprint(('Mean salary for people in the same state'))
    pprint(('Average salary for occupation'))
    pprint(('Mean salary for occupation'))
