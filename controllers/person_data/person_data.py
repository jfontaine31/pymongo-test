from faker import Faker
import random
from random import randint
from datetime import datetime, date

fake = Faker()


def get_person_data(newSeed):
    if newSeed is not None:
        fake.seed(newSeed)
        random.seed(newSeed)

    age = randint(1, 120)
    birthday = get_birthday(age)
    person = {
        'first_name': fake.first_name(),
        'last_name': fake.last_name(),
        'age': age,
        'birthday': birthday,
        'phone': fake.phone_number(),
        "address": {
            'street_address': fake.street_address(),
            'zip_code': fake.postalcode(),
            'state': fake.state()
        },
        'occupation': {
            'company_name': fake.company(),
            'job': fake.job(),
            'income': randint(15000, 150000)

        },
        'created_on': datetime.now()
    }
    return person


def get_birthday(age):
    curr_date = datetime.now()
    curr_year = curr_date.year
    year = curr_year - age
    month = day = 0
    birthday = None
    while not birthday:
        try:
            month = randint(1, 12)
            day = randint(1, 31)
            birthday = datetime(year, month, day)
        except:
            pass

    return datetime(year, month, day)
