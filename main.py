import os
from pymongo import MongoClient
from controllers import get_person_data, executeQueries


def get_mongo_client(MONGO_URL):
    mongo_client = MongoClient(MONGO_URL)
    return mongo_client


def get_db(MONGO_URL, db_name='admin'):
    mongo_client = get_mongo_client(MONGO_URL)
    db = mongo_client[db_name]
    return db


def reset_collection(collection):
    return collection.delete_many({})


def insert_people(collection, amount, seed):
    people = [get_person_data(amount*seed) for amount in range(0, amount)]
    return collection.insert_many(people)




def app():
    MONGO_URL = os.environ['MONGO_URL']
    if not MONGO_URL:
        print('MONGO_URL needs to be set')
        exit(1)
    db = get_db(MONGO_URL)
    people_collection = db.people
    #  RESET COLLECTION
    # print('reset people collection', reset_collection(people_collection).deleted_count)
    #  INSERT PEOPLE
    # print('inserted:', insert_people(people_collection, 10000, seed=1000).acknowledged)
    executeQueries(people_collection)

def main():
    print('app has started')
    app()


if __name__ == '__main__':
    main()
