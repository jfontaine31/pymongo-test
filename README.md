# Purpose
    The purpose of the codebase is to just learn pymongo 
    Ask some interesting questions with my data an answer them
    Without worry about conventions but just learning
    
    More later stages is to convert this codebase
    into a data api service to be consumed 

#  Possible queries
* How many people work at the same company
* list of people at said age
* people with the same first name
* people with the same last name
* people with the same first name and last name
* people with income less than
* people with income greater than
* mean income of population in the same state
* population of with income
* people with the same birthday
* counts of the same 
* What is the Distinct list of the different companies 
* What is the Distinct list of occupations
* Average salary for people in the same state
* Mean salary for people in the same state
* Average salary at said company
* Mean salary at said company
* Average salary for occupation
* Mean salary for occupation


# Running Mongo
    mongo --host 127.0.0.1:27017 
    